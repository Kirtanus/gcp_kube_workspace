# About image #

This image is useful for working in GCP+Kubernetes environment.
It is based on fresh google/cloud-sdk slim version with nice kube cli tools and autocompletion (more information here : <https://itnext.io/kubernetes-command-line-tools-acad11683794>)

Also featuring Helm, Vault and Terraform.

And much more :) See below.

## Details ##

* Base image: google/cloud-sdk:273.0.0-slim
* Entrypoint to Bash shell
* kubectl:
  * k alias
  * bash autocompletion  
* kubectx, kubens and fzf
  * ctx and ns aliases
* kube-ps1 enabled
* Fresh docker-compose
* Extra cli tools
  * thefuck
  * tmux
  * ack
  * unzip
  * vim
  * wget
  * jq
* cfssl+cfssljson: 1.4.1
* Hashicorp Vault: 1.7.1
* Hashicorp Terraform: 0.15.1
* Helm: v3.5.4
* istioctl 1.4.2 - disabled
* glooctl: v1.2.15 - disabled
* Docker client configured to use Docker on Windows engine by default
  * Do not forget to enable "Expose daemon on tcp://localhost:2375 without TLS" in general setting on Windows host.
